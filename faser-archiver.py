#!/usr/bin/env python3

#
# Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
# 

# Author : Brian Petersen 
# 
# Description : Used to automatically archive recorded data files

import getopt
import json
import os
import platform
import re
import requests
import subprocess
import sys

runCache={}

def runOngoing(runNumber):
    global runCache

    if not runNumber in runCache: 
        r = requests.get(f'http://faser-runnumber.web.cern.ch/RunInfo/{runNumber}')
        if r.status_code==200:
            runResult=r.json()
            runCache[runNumber]=runResult["starttime"]==runResult["stoptime"]
        else:
            runCache[runNumber]=False
        
    return runCache[runNumber]

def fileIsOpen(fileName):
    rc,output = subprocess.getstatusoutput(f"fuser {fileName}")
    return rc==0

class fileArchiver:
    def __init__(self,fileName, archiveDir, archiveServer, runNumber, stream):
        self.fullName = fileName
        self.server = archiveServer
        self.destDir = os.path.join(archiveDir,"Run-"+runNumber)
        self.destFile = os.path.join(self.destDir,os.path.basename(fileName))
        self.size = os.path.getsize(fileName)
        self.hash = self.fileHash(fileName)
        self.runNumber = runNumber
        self.stream = stream

    def createDest(self):
        if not os.access(self.destDir,os.W_OK):
            os.mkdir(self.destDir)
    
    def fileHash(self,fileName):
        rc,hashline = subprocess.getstatusoutput(f"xrdadler32 {fileName}")
        if rc:
            print("Failed to get hash",rc,hashline)
            hashvalue=""
        else:
            hashvalue=hashline.split()[0]
        return hashvalue

    def remoteHash(self,fileName):
        rc,hashline = subprocess.getstatusoutput(f"xrdfs {self.server} query checksum {fileName}")
        if rc:
            print("Failed to get hash",rc,hashline)
            hashvalue="None"
        else:
            hashvalue=hashline.split()[1]
        return hashvalue

    def checkArchivedFile(self,isRunning=False):
        if not os.access(self.destFile,os.R_OK): return False
        archiveSize=os.path.getsize(self.destFile)
        if archiveSize>self.size: raise Exception(f"Archived file is larger than data file")
        if archiveSize!=self.size: return False
        if not isRunning and self.remoteHash(self.destFile)!=self.hash: return False
        return True

    def copyFile(self):
        self.createDest()
        return subprocess.getstatusoutput(f"xrdcp -s --xrate 50m --cksum auto:source --rm-bad-cksum {self.fullName} root://{self.server}/{self.destFile}")

    def removeFile(self):
        return subprocess.getstatusoutput(f"rm {self.fullName}")
        
    def registerFile(self,influxdb):
        host=platform.node()
        outFH=open("/tmp/archive-upload.dat","w")
        outFH.write(f'DataFiles,Run=Run-{self.runNumber},Stream={self.stream},Host={host} path="{self.destFile}",size={self.size},hash="{self.hash}"\n')
        outFH.close()
        return subprocess.getstatusoutput(f"curl -s -i -XPOST '{influxdb}' --data-binary @/tmp/archive-upload.dat")

def logFileCount(influxdb,copied,registered):
    host=platform.node()
    outFH=open("/tmp/archive-upload.dat","w")
    outFH.write(f'FileTransfers,Host={host} Transfers={copied},Registered={registered}\n')
    outFH.close()
    return subprocess.getstatusoutput(f"curl -s -i -XPOST '{influxdb}' --data-binary @/tmp/archive-upload.dat")

def usage():
    print("faser-archiver.py [-c configFile]")
    sys.exit(2)

def main(args):
    configFile = "data-archiver.json"

    try:
        opts, args = getopt.getopt(args,"c:",[])
    except getopt.GetoptError:
        usage()
    for opt,arg in opts:
        if opt=="-c":
            configFile = arg
    
    try:
        config=json.load(open(configFile))
    except FileNotFoundError:
        print(f"ERROR: Did not find config file {configFile}")
        sys.exit(1)
    except json.decoder.JSONDecodeError as error:
        print(f"ERROR: Failure reading config file {configFile}:\n  ",error)
        sys.exit(1)

    rc,output=subprocess.getstatusoutput(f"echo {config['password']}|kinit {config['account']} && eosfusebind")
    if rc:
        print("ERROR: failed to get EOS access")
        sys.exit(1)

    pattern=re.compile(config["filePattern"])
    fileNames=os.listdir(config["inputDir"])[:5]  #max 20 files per copy for now

    numTransferred=0
    numRegistered=0
    for name in fileNames:
        m=re.match(pattern, name)
        if not m:
            print(f"WARNING: Unknown file on data disk: {name}")
            continue
        if len(m.groups())!=2:
            print(f"ERROR: Did not get stream and run number from {name}: ",m.groups())
            sys.exit(1)
        stream,runnumber=m.groups()

        fullName=os.path.join(config["inputDir"], name)

        if fileIsOpen(fullName): 
            continue

        archive=fileArchiver(fullName, config["archiveDir"], config["archiveServer"],
                             runnumber, stream)
        if not archive.checkArchivedFile(runOngoing(runnumber)):
            rc,output=archive.copyFile()
            if rc:
                print(f"ERROR: Failed to copy over {fullName}",output)
                continue
            if not archive.checkArchivedFile():
                print(f"ERROR: Copied file does not match {fullName}")
                continue
            numTransferred+=1
        if not runOngoing(runnumber):
            rc,error=archive.registerFile(config['influxdb'])
            if rc:
                print(f"ERROR: failed to register {fullName}:",rc,error)
                continue
            numRegistered+=1

            rc,error=archive.removeFile()
            if rc: 
                print(f"ERROR: failed to remove {fullName}: {error}")
                continue
                
    rc,error=logFileCount(config['influxdb'],numTransferred,numRegistered)
    if rc:
        print(f"ERROR: failed to log statistics:",error)
        sys.exit(1)

if __name__ == "__main__":
   main(sys.argv[1:])
